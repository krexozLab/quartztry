package com.example.quartztry.timerservices;


import com.example.quartztry.info.TimerInfo;
import com.example.quartztry.util.TimerUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SchedulerService {

    private final Scheduler scheduler;

    @Autowired
    public SchedulerService(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @SneakyThrows
    public void schedule(final Class jobClass, final TimerInfo info) {
        final JobDetail jobDetail = TimerUtils.buildJobDetail(jobClass, info);
        final Trigger trigger = TimerUtils.buildTrigger(jobClass, info);

        scheduler.scheduleJob(jobDetail, trigger);
    }

    public List<TimerInfo> getAllRunningTimers() {
        try {
            return scheduler.getJobKeys(GroupMatcher.anyGroup())
                    .stream()
                    .map(jobKey ->{
                        try {
                            final JobDetail jobDetail = scheduler.getJobDetail(jobKey);
                            return (TimerInfo) jobDetail.getJobDataMap().get(jobKey.getName());
                        } catch (SchedulerException e) {
                            log.error(e.getMessage());
                            return null;
                        }
                    }).filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @SneakyThrows
    public TimerInfo getRunningTimer(String timerId) {
        final JobDetail jobDetail = scheduler.getJobDetail(new JobKey(timerId));

        if (jobDetail == null){
            return null;
        }

        return (TimerInfo) jobDetail.getJobDataMap().get(timerId);
    }

    @SneakyThrows
    public void updateTimer(final String timerId, final TimerInfo info){
        final JobDetail jobDetail = scheduler.getJobDetail(new JobKey(timerId));

        if (jobDetail == null){
            return;
        }

        jobDetail.getJobDataMap().put(timerId, info);
    }

    @SneakyThrows
    public Boolean deleteTimer(final String timerId) {
        return scheduler.deleteJob(new JobKey(timerId));
    }

    @SneakyThrows
    @PostConstruct
    public void init() {
        scheduler.start();
        scheduler.getListenerManager().addTriggerListener(new SimpleTriggerListener(this));
    }

    @SneakyThrows
    @PreDestroy
    public void preDestroy(){
        scheduler.shutdown();
    }
}
