package com.example.quartztry.advicer;


import org.quartz.ObjectAlreadyExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvicer extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ObjectAlreadyExistsException.class)
        public ResponseEntity handleObjectAlreadyExistsException(ObjectAlreadyExistsException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseMessage("The same job is already running"));
    }

    @ExceptionHandler(NullPointerException.class)
        public ResponseEntity handleNullPointerException(NullPointerException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseMessage("Sorry something went wrong"));
    }

    @ExceptionHandler(ClassNotFoundException.class)
        public ResponseEntity handleNotFoundException(ClassNotFoundException e) {
        return  ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseMessage("Error 404: WAS NOT FOUND"));
    }
}
