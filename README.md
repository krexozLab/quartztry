# //\\\ QuartzTry //\\\

## Using Quartz to Schedule Jobs.

>>How to use the app:
> - GET METHODS :
> localhost:8090/home/info/timers & /home/info/{timerId}
> - POST METHODS :
> localhost:8090/home/start
> - DELETE METHODS :
> localhost:8090/home/info/{timerId}
> 
> The timerId for FirstJob it's actual the class name  "FirstJob".
> 
>> How to start the FirstJob ( output : "Remaining fire count is: " )
>> - The POST methods requires no RequestBody just the send request.
>> 1. Once you POST a request to /home/start it will start the job.
>> 2. The output it's in console via log.info and it's called 5 times. (5 fire counts)
>> 
>> TIPS : The fire counter ends with 0 not 1, 0 it's counter as a starting for the counter.
>
>> Requesting Timer Informations via GET :
>>- All Timers Informations (Any Group) if you add more jobs in : /home/info/timers
>>
>>- One Timer by their timerId you define in : /home/info/{timerId}
>
>> Deleting (STOPING) a running Job via DELETE :
>>- Delete the running job by its timerId at : /home/info/{timerId}
>>
>> Returning *true* and stops the Job if it's running and *false* if it's not running.
> 
>> Dependencies used:
>> - Quartz Starter
>> - Lombok ( optimising the code )
>> - Web Starter