package com.example.quartztry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuartztryApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuartztryApplication.class, args);
    }

}
