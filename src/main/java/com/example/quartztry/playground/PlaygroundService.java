package com.example.quartztry.playground;

import com.example.quartztry.info.TimerInfo;
import com.example.quartztry.jobs.FirstJob;
import com.example.quartztry.timerservices.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaygroundService {
    private final SchedulerService schedulerService;

    @Autowired
    public PlaygroundService(final SchedulerService schedulerService) {
        this.schedulerService = schedulerService;
    }

    public void runHelloWorldJob(){
        final TimerInfo info = new TimerInfo();
        info.setTotalFireCount(5);
        info.setRemainingFireCount(info.getTotalFireCount());
        info.setRepeatIntervalMs(2000);
        info.setInitialOffsetMs(1000);
        info.setCallbackData("");

        schedulerService.schedule(FirstJob.class, info);
    }

    public Boolean deleteTimer(final String timerId) {
       return schedulerService.deleteTimer(timerId);
    }

    public List<TimerInfo> getAllRunningTimers() {
         return schedulerService.getAllRunningTimers();
    }

    public TimerInfo getRunningTimer(String timerId) {
        return schedulerService.getRunningTimer(timerId);
    }
}
