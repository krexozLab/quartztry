package com.example.quartztry.playground;


import com.example.quartztry.info.TimerInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/home")
public class PlaygroundController {

    @Autowired
    private PlaygroundService playgroundService;

    @PostMapping("/start")
    public void runHelloWorld(){
        playgroundService.runHelloWorldJob();
    }

    @GetMapping("/info/timers")
    public List<TimerInfo> getAllRunningTimers() {
        return playgroundService.getAllRunningTimers();
    }

    @GetMapping("/info/{timerId}")
    public TimerInfo getRunningTimer(@PathVariable String timerId){
        return playgroundService.getRunningTimer(timerId);
    }

    @DeleteMapping("/delete/{timerId}")
    public Boolean deleteTimer(@PathVariable String timerId) {
        return playgroundService.deleteTimer(timerId);
    }
}
